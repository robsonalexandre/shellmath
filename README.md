# shellmath

Uma coleção de funções matemáticas e aritméticas para o Bash usando apenas bash e awk.

Este é um projeto didático proposto para os alunos do [Curso Básico de Programação em Bash](https://cursos.debxp.org) e membros da [comunidade debxp](https://debxp.org).

## Uso

A ideia é que o arquivo `shellmath` seja inserido no '.bashrc' do usuário, tornando as funções disponíveis na linha de comando, ou inserido por `source` em scripts.

## Desafios iniciais

* [x] **calc** - Efetua expressões aritméticas
* [x] **maxvalb** - Calcula o maior inteiro possível dado o número de bytes
* [x] **isprime** - retorna sucesso se o número for primo (@robsonalexandre)
* [ ] **isnumber** - verifica se a string é numérica
* [ ] **isint** - verifica se a string expressa um inteiro
* [ ] **isdoub** - verifica se a string expressa um número de ponto flutuante válido
* [ ] **isfib** - verifica se o inteiro pertence à série de fibonacci
* [ ] **isbin** - verifica se o número é um binário válido
* [ ] **isoct** - verifica se o número é um octal válido
* [ ] **ishex** - verifica se o número é um hexadecimal válido
* [ ] **bconv** [bodh] NUM - converte NUM para a base em $1



